﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Athlete 
{


	public string Name
	{
		get{ return m_name; }
	}

	public Sprite Avatar
	{
		get{ return m_avatar; }
	}

	public int StarRating
	{
		get{ return m_starRating; }
	}

	public string Country
	{
		get{ return m_country; }
	}

	public string LastPlayed
	{
		get{ return m_lastPlayed; }
	}

	public List<Positions> positions
	{
		get{ return m_positions; }
	}

	public bool IsInjured
	{
		get{ return m_isInjured; }
	}

	public string Id
	{
		get{ return m_id; }
	}

	public bool avatarLoaded;

	[SerializeField]
	private string m_name;

	[SerializeField]
	private Sprite m_avatar;

	[SerializeField]
	private int m_starRating;

	[SerializeField]
	private string m_country;

	[SerializeField]
	private string m_lastPlayed;

	[SerializeField]
	private List<Positions> m_positions;

	[SerializeField]
	private bool m_isInjured;

	[SerializeField]
	private string m_id;


	public Athlete(Dictionary<string,object> dic)
	{
		m_name = dic [Globals.name].ToString();
		m_starRating = int.Parse(dic [Globals.starRating].ToString());
		m_country = dic [Globals.country].ToString ();
		m_lastPlayed = dic [Globals.lastPlayed].ToString();
		m_isInjured = bool.Parse (dic[Globals.isInjured].ToString());
		m_id = dic [Globals.id].ToString();

		m_positions = new List<Positions> ();

		List<object> lis = (List<object>)dic [Globals.positions];

		foreach (object s in lis)
		{
			Positions p = (Positions) Enum.Parse(typeof(Positions), s.ToString().Replace("-",""), true);
			m_positions.Add (p);
		}

		Utils.Instance.LoadSpriteFromURL (dic [Globals.avatarUrl].ToString (), (sp) => {
			m_avatar = sp;
			avatarLoaded = true;
		});
			
	}



}
