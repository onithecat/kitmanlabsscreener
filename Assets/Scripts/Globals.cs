﻿public class Globals 
{
	public static string fileName = "athletes";

	public static string name = "name";
	public static string id = "id";
	public static string avatarUrl = "avatar_url";
	public static string starRating = "star_rating";
	public static string country = "country";
	public static string lastPlayed = "last_played";
	public static string positions = "positions";
	public static string isInjured = "is_injured";

}
