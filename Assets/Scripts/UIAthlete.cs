﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIAthlete : MonoBehaviour 
{
	Athlete athlete;

	public Image avatar;
	public Text name;
	public Text country;
	public Text rating;
	public Text id;
	public Text isInjured;
	public Text positions;

	private PlayerMarker marker;
	private bool dragging;

	public void Init(Athlete a)
	{
		athlete = a;

		name.text = a.Name;

		for(int i = 0; i < a.StarRating; i++)
		{
			rating.text += "X";	
		}

		id.text = a.Id;

		country.text = a.Country;

		isInjured.text = a.IsInjured.ToString();

		if (a.IsInjured)
			GetComponent<Image> ().color = Color.red;

		for (int i = 0; i < a.positions.Count; i++)
		{
			positions.text += a.positions [i]+"  ";
		}

		if (a.avatarLoaded)
		{
			avatar.sprite = a.Avatar;
		} else
		{
			StartCoroutine(ShowAvatar());
		}

		AddActions ();
	}

	void BeginDrag()
	{
		if (athlete.IsInjured)
			return;
		
		dragging = true;
		transform.SetParent (MenuManager.Instance.UIRoot);
	}

	void Drag()
	{
		if (!dragging)
			return;
		
		transform.position = Vector3.Lerp(transform.position , Input.mousePosition, 0.5f);
	}

	void EndDrag()
	{
		if (!dragging)
			return;
		
		dragging = false;

		if (marker == null)
		{
			transform.SetParent (MenuManager.Instance.availableAthletesPanel);
		} else
		{
			transform.position = marker.transform.position;
			marker.athleteId = athlete.Id;
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if ((marker = coll.gameObject.GetComponent<PlayerMarker> ()) != null)
		{
			if (!athlete.positions.Contains (marker.position) || marker.athleteId != "")
			{
				marker = null;
			}
		}
	}

	void OnCollisionExit2D(Collision2D coll)
	{
		if (marker != null && marker.athleteId == athlete.Id)
		{
			marker.athleteId = "";
			marker = null;
		}
	}

	void AddActions()
	{
		EventTrigger trigger = GetComponent<EventTrigger>();
		EventTrigger.Entry entry = new EventTrigger.Entry();
		entry.eventID = EventTriggerType.BeginDrag;
		entry.callback.AddListener( (eventData) => { BeginDrag(); } );
		trigger.triggers.Add(entry);

		EventTrigger.Entry entry2 = new EventTrigger.Entry();
		entry2.eventID = EventTriggerType.Drag;
		entry2.callback.AddListener( (eventData) => { Drag(); } );
		trigger.triggers.Add(entry2);

		EventTrigger.Entry entry3 = new EventTrigger.Entry();
		entry3.eventID = EventTriggerType.EndDrag;
		entry3.callback.AddListener( (eventData) => { EndDrag(); } );
		trigger.triggers.Add(entry3);
	}


	IEnumerator ShowAvatar()
	{

		yield return new WaitUntil (() => athlete.avatarLoaded);

		avatar.sprite = athlete.Avatar;
	
	}
}
