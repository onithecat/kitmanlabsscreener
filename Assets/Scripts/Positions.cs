﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Positions 
{
	Prop,
	Hooker,
	SecondRow,
	LooseForward,
	ScrumHalf,
	StandOff,
	Centre,
	Fullback,
	Lock,
	Winger,
	OutHalf,
	Flanker,
	NumberEight
}
