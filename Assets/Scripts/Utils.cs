﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Utils : MonoBehaviour
{
	public static Utils Instance;

	void Awake()
	{
		Instance = this;
	}

	public static string LoadResourceTextfile(string path)
	{
		string filePath = path.Replace(".json", "");

		TextAsset targetFile = Resources.Load<TextAsset>(filePath);

		return targetFile.text;
	}

	public void LoadSpriteFromURL(string url, Action<Sprite> onComplete)
	{
		StartCoroutine (DownloadSprite (url, onComplete));
	}

	IEnumerator DownloadSprite(string url, Action<Sprite> onComplete) 
	{
		WWW www = new WWW(url);
		yield return www;
		onComplete(Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0)));
	}
}
