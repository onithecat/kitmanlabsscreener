﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MiniJSON;
using System.Linq;
using System;

public class MenuManager : MonoBehaviour {

	private List<Athlete> athletes;

	public UIAthlete uiAthtletePrefab;

	public Transform UIRoot;
	public Transform availableAthletesPanel;

	public static MenuManager Instance;

	void Awake()
	{
		Instance = this;
	}

	void Start()
	{
		LoadAthletes ();
		ShowAthletes ();
	}

	private void LoadAthletes()
	{
		string athletesJson = Utils.LoadResourceTextfile (Globals.fileName);

		athletes = new List<Athlete> ();

		var dic = MiniJSON.Json.Deserialize (athletesJson) as Dictionary<string,object>;

		foreach (KeyValuePair<string, object> kvp in dic)
		{
			foreach (object obj in (List<object>)kvp.Value)
			{
				athletes.Add(new Athlete ((Dictionary<string,object>)obj));
			}
		}
	}

	private void ShowAthletes()
	{
		foreach(Athlete a in athletes)
		{
			
			UIAthlete uiAthlete = Instantiate (uiAthtletePrefab)as UIAthlete;
			uiAthlete.gameObject.SetActive (true);
			uiAthlete.Init (a);
			uiAthlete.transform.SetParent (availableAthletesPanel);
		}
	}
}
